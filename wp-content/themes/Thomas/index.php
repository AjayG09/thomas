<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @package WordPress
* @subpackage Thomas
* @since 1.0.0
*/

get_header();
?>

<section class="content-area">
<main class="site-main">

<?php
if ( have_posts() ) {
	
	// Load posts loop.
	while ( have_posts() ) {
		the_post(); ?>
		<article <?php post_class(); ?>>
			<div class="banner">
				<div class="wrapper">
					<?php
					the_title( '<h2 class="banner__title">', '</h2>' );
					?>
				</div>
			</div><!-- .entry-header -->
			
			<div class="content">
				<div class="wrapper">
					<?php	echo wpautop( get_the_content());?>
				</div>
			</div><!-- .entry-content -->
		</article><!-- #post-${ID} -->
				
	<?php }
				
				
	} 
	
?>
				
</main><!-- .site-main -->
</section><!-- .content-area -->

<?php
get_footer();
