<?php 
$content = get_field('subscribe_content', 'option');
if($content):
?>
<div class="subscribe">
	<?php echo $content; ?>
</div>
<?php endif; ?>