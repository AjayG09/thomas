<?php if( have_rows('languages', 'option') ) : ?>
<ul class="languages">
	<?php 
	while ( have_rows('languages', 'option') ) : the_row();
		$lang	= get_sub_field('language');
		$link	= get_sub_field('lang_links');
		if($link) :
	?>
	<li>
		<a href="<?php echo ($link['ulr']) ? $link['ulr'] : '#FIXME'; ?>" title="<?php echo ($link['title']) ? $link['title'] : '#FIXME'; ?>"><?php echo ($lang) ? $lang : 'Language'; ?></a>
	</li>	
<?php 
		endif;
	endwhile; ?>
</ul>
<?php endif; ?>