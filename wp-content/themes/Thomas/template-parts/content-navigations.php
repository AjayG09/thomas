<?php if( has_nav_menu( 'primary-menu' ) ) : ?>
<div class="nav-details">
	<nav class="navbar">
		<?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'menu')); ?>
	</nav>
</div>
<?php endif; ?>
