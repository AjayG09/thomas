<?php 
	$search_icon = get_field('search_field', 'option');
	$search_link = get_field('search_link', 'option'); 
if($search_icon || $search_link) :
?>
<div class="search">
	<a href="<?php echo ($search_link['url']) ? $search_link['url'] : '#FIXME'; ?>" title="<?php echo ($search_link['title']) ? $search_link['title'] : 'Search'; ?>">
		<img src="<?php echo $search_icon['url']; ?>" alt="<?php echo $search_icon['alt']; ?>">
	</a>
</div>
<?php endif; ?>