<?php 
	$logo = get_field('site_logo', 'option');

if($logo) : 
?>
<h1 class="header-logo">
	<a href="<?php echo get_home_url(); ?>" title="<?php echo( ($logo['alt']) ? $logo['alt'] : 'Thomas' ); ?>">
	<img src="<?php echo $logo['url']; ?>" alt="<?php echo( ($logo['alt']) ? $logo['alt'] : 'Logo' ); ?>" class="site-logo">
	</a>
</h1>
<?php  
endif;
?>