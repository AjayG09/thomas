<?php 
if( have_rows('social_links', 'option') ) : ?>
<ul class="social-links">
<?php 
while ( have_rows('social_links', 'option') ) : the_row();
	$icon	= get_sub_field('soical_icons');
	$link	= get_sub_field('social_link');?>
	<li>
		<a href="<?php echo ($link['url']) ? $link['url'] : '#FIXME'; ?>"	title="<?php echo ($link['title']) ? $link['title'] : 'Link'; ?>" <?php echo ($link['target']) ? 'traget="'.$link['target'].'"' : ''; ?>>
			<img src="<?php echo $icon['url']; ?>" alt="<?php echo ($icon['alt']) ? $icon['alt'] : 'Social'; ?>">
		</a>
	</li>
<?php endwhile; ?>
</ul>
<?php endif; ?>
