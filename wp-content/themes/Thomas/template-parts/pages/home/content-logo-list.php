<?php 
if( have_rows('logo_list', $post->ID) ) : ?>
<section class="logo-section">
	<div class="wrapper">
		<ul class="logo-list">
			<?php 
			while( have_rows('logo_list') ) : the_row();
				$icon = get_sub_field('logo_icon');
				$link = get_sub_field('logo_link');
				if($link) : ?>

		 	<li>
		 		<a href="<?php echo ($link['url']) ? $link['url'] : '#FIXME'; ?>" title="<?php echo ($link['title']) ? $link['title'] : 'Logo';  ?>">
		 			<?php if($icon) : ?>
		 			<img src="<?php echo $icon['url'];  ?>" alt="<?php echo $icon['alt'];  ?>">
		 			<?php endif; ?>
		 		</a>
		 	</li>

			<?php 
				endif; 
			endwhile; ?>
		</ul>
	</div>
</section>
<?php endif; ?>