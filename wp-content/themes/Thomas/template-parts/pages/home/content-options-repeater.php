<?php 
if( have_rows('options_repeater', $post->ID) ): ?>
<div class="options-list">
	<div class="wrapper">
		<?php 
		while( have_rows('options_repeater',  $post->ID) ) : the_row();
			$logo = get_sub_field('options_logo');
			$content = get_sub_field('options_content');
			if($content) :
		?>
		<div class="option">
			<?php if($logo) : ?>
			<figure>
				<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'];  ?>">
			</figure>
			<?php endif; ?>
			<div class="content">
				<?php echo wpautop($content); ?>
			</div>
		</div>
		<?php endif; 
			endwhile;	?>
	</div>
</div>
<?php endif; ?>
