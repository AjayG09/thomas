<?php 
$search_heading = get_sub_field('search_heading', $post->ID);
$conditions = get_sub_field('condition', $post->ID);
$makes = get_sub_field('makes', $post->ID);
$types = get_sub_field('type', $post->ID);

if($condition || $makes || $type) :
?>
<div class="search-section">
	<?php if($search_heading) : ?>
	<h3><?php echo $search_heading; ?></h3>
	<?php endif; ?>
	<form action="#FIXME" type="POST">
		<select>
			<?php foreach ($conditions as $condition) : ?>
		  <option value="<?php echo $condition; ?>"><?php echo $condition['select_condition']; ?></option>
			<?php endforeach; ?>
		</select>
		<select>
			<?php foreach ($makes as $make) : ?>
		  <option value="<?php echo $makes; ?>"><?php echo $make['make_select']; ?></option>
			<?php endforeach; ?>
		</select>
		<select>
			<?php foreach ($types as $type) : ?>
		  <option value="<?php echo $types; ?>"><?php echo $type['type_select']; ?></option>
			<?php endforeach; ?>
		</select>
		<input type="submit" name="submit">
	</form>
</div>
<?php endif; ?>