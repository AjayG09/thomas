<?php 
$featured_list = get_sub_field('featured_list', $post->ID);
$featured_heading = get_sub_field('featured_content', $post->ID);
if(!empty($featured_list)) :
?>
<section class="featured-section">
	<div class="wrapper">
		<?php if($featured_heading) : ?>
		<h3><?php echo $featured_heading; ?></h3>
		<?php endif; ?>
		<ul class="featured-list">
			<?php foreach ($featured_list as $featured) : 
				$permalink = get_permalink($featured->ID);
				$title = get_the_title($featured->ID);
				$length = get_field('boat_length', $featured->ID);
			if($title) :
			?>
			<li>
				<a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>">
					<?php if( has_post_thumbnail($featured->ID) ) : ?>
					<figure>
						<?php echo get_the_post_thumbnail($featured->ID); ?>
					</figure>
					<?php endif; ?>
					<div class="featured-content">
						<h4><?php echo $title; ?></h4>
						<?php if($length) : ?>
						<p>length: <?php echo $length; ?></p>
						<?php endif; ?>
					</div>
				</a>
			</li>
		<?php 
			endif;
		endforeach; ?>
		</ul>
	</div>
</section>
<?php endif; ?>
