<?php 
$hero_content = get_field('hero_content', $post->ID);
$hero_img = get_field('hero_image', $post->ID);
$hero_mobile_img = get_field('hero_mobile_image', $post->ID);
if($hero_content) :
?>
<section class="hero-section">
	<div class="wrapper">
		<div class="hero-content">
			<?php echo $hero_content; ?>
		</div>
		<?php if($hero_img || $hero_mobile_img) : ?>
		<figure class="hero-background">
			<img class="desktop-img" src="<?php echo $hero_img['url']; ?>" alt="<?php echo $hero_img['alt']; ?>">
			<img class="mobile-img" src="<?php echo $hero_mobile_img['url']; ?>" alt="<?php echo $hero_mobile_img['alt']; ?>">
		</figure>
	<?php endif; ?>
	</div>
</section>
<?php endif;  ?>