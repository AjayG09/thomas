<?php 
$special_list = get_sub_field('special_list', $post->ID);
$special_heading = get_sub_field('special_heading', $post->ID);
if(!empty($special_list)) :
?>
<section class="special-section">
	<div class="wrapper">
		<h5><?php echo $special_heading; ?></h5>
		<ul class="special-list">
			<?php foreach ($special_list as $special) : 
				$after = get_field('after_price', $special->ID);
				$before = get_field('before_price', $special->ID);
				$length = get_field('boat_length', $special->ID);
				$title = get_the_title($special->ID);
				$permalink = get_the_title($special->ID);
			
			if($title) : 
			?>
			<li>
				<a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>">
					<?php if( has_post_thumbnail($special->ID) ) : ?>
					<figure>
						<?php echo get_the_post_thumbnail($special->ID); ?>
					</figure>
					<?php endif; ?>
					<div class="special-content">
						<div class="price">
						<?php if( $after ): ?>
							<span class="after"><?php echo $after; ?></span>
						<?php endif; ?>
						<?php if( $before ): ?>
							<del class="before"><?php echo $before; ?></del>
						<?php endif; ?>
						</div>
						<h4><?php echo $title; ?></h4>
						<?php if( $length ) : ?>
						<p>length: <?php echo $length; ?></p>
						<?php endif; ?>
					</div>
				</a>
			</li>
		<?php 
			endif;
			endforeach; ?>
		</ul>		
	</div>
</section>
<?php endif; ?>
