<?php 
	$content = get_sub_field('content', $post->ID);
	$para = get_sub_field('content', $post->ID);
	$id = get_sub_field('youtube_video_id', $post->ID);
if($content || $id) :
?>
<section class="row-with-video">
	<div class="wrapper">
		<?php echo wpautop($content); ?>
		<div class="video" id="<?php echo $id; ?>">
			<div id="video-placeholder"></div>
		</div>
	</div>
</section>
<?php endif; ?>
