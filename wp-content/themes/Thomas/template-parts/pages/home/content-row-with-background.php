<?php 
$content = get_sub_field('content_overlay', $post->ID);
$background_image = get_sub_field('background_image', $post->ID);

if($content) :
?>
<section class="row-with-bgimage">
	<div class="content">
		<?php echo $content; ?>
	</div>
	<?php if($background_image) : ?>
	<figure>
		<img src="<?php echo $background_image['url']; ?>" alt="<?php echo $background_image['alt']; ?>">
	</figure>
	<?php endif; ?>
</section>
<?php endif; ?>
