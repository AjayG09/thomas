<?php 
$links = get_field('footer_links', 'option');
if( have_rows('footer_links', 'option') ) :
?>
<div class="footer-links">
	<?php
		while ( have_rows('footer_links', 'option') ) : the_row(); 
		$link = get_sub_field('footer_link');
	?>
	<a href="<?php echo ($link['url']) ? $link['url'] : '#FIXME'; ?>"	title="<?php echo ($link['title']) ? $link['title'] : 'Link'; ?>" <?php echo ($link['target']) ? 'traget="'.$link['target'].'"' : ''; ?>><?php echo ($link['title']) ? $link['title'] : 'Link'; ?></a>
	<?php endwhile; ?>
</div>
<?php endif; ?>
	