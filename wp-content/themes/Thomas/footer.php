<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package WordPress
 * @subpackage Thomas
 * @since 1.0.0
 */

$social_links = get_field('social_links','options');
?>
	<!-- footer starts here -->
	<footer class="scrollable-section">
		<div class="wrapper">
			<?php
				get_template_part('template-parts/content', 'navigations');
				get_template_part('template-parts/content', 'social-links');
				get_template_part('template-parts/content', 'footer-links');
				get_template_part('template-parts/content', 'subscribe');
			?>
		</div>
	</footer>
	<!-- footer ends here -->
	<?php
	/* Always have wp_footer() just before the closing </body>
	* tag of your theme, or you will break many plugins, which
	* generally use this hook to reference JavaScript files.
	*/
	wp_footer();
	?>
	<script>
		if($('body').hasClass('home page-template')) {
			var tag = document.createElement('script');

	    tag.src = "https://www.youtube.com/iframe_api";
	    var firstScriptTag = document.getElementsByTagName('script')[0];
	    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			var player;
			var youtube_video = $('.row-with-video .video').attr('id');
			function onYouTubeIframeAPIReady() {
			    player = new YT.Player('video-placeholder', {
			        width: 600,
			        height: 400,
			        videoId: youtube_video,
			    });
			}
		}
	</script>
</body>
</html>
