<?php 
/**
 * Thomas functions and definitions
 *
 * @package WordPress
 * @subpackage thomas
 * @since 1.0.0
 */


if ( ! function_exists( 'thomas_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thomas_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on thomas, use a find and replace
		 * to change 'thomas' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'thomas' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		register_nav_menus( 
			array(
				'primary-menu' =>  __( 'Primary Menu', 'thomas' ), 
			)
		);

		//Add support for featured image
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 ********************************************************************************/
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 ********************************************************************************/
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'status',
			'audio'
		) );
	}
endif;
add_action( 'after_setup_theme', 'thomas_setup' );


/**
 * Enqueue scripts and styles.
 */
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', "https://code.jquery.com/jquery-3.3.1.min.js", false, null);
	wp_enqueue_script('jquery');
}

function thomas_scripts() {
	// Theme stylesheet.
	wp_enqueue_style( 'thomas-style', get_stylesheet_uri().'?'.time() );
	wp_enqueue_style('slick-slider', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
	wp_enqueue_style('slick-slider-theme', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css');
	wp_enqueue_script('slick-script','https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',array('jquery'), true );
	wp_enqueue_script( 'thomas-script', get_theme_file_uri( '/js/script.js' ), array( 'jquery' ), '1.0', true );

	//Localisation
	wp_localize_script('thomas-script', 'url', array(
		'siteurl' => home_url( $wp->request ),
		'adminurl' => admin_url( 'admin-ajax.php' )
	));
}
add_action( 'wp_enqueue_scripts', 'thomas_scripts' );

/*
* Disable Gutenberg Editor
*/
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/**  When using the WP function __() to translate the options page’s settings, 
* please be aware that your translations may not yet be loaded. To ensure your translations have been loaded, 
* run the acf_add_options_page() function within the ‘init’ or ‘acf/init’ actions.
*/
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}

//add SVG to allowed file uploads
function fix_avada_mime_types($mime){
	$mime['svg']='image/svg+xml';
	return $mime;
}
add_filter('upload_mimes', 'fix_avada_mime_types', 99);

// Our custom post type function
function create_posttype() {
  register_post_type( 'boat',
  // CPT Options
    array(
      'labels' => array(
          'name' => __( 'Boat' ),
          'singular_name' => __( 'Boat' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'boats'),
      'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    )
  );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
?>
