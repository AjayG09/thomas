<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and <header> section everything up until <div id="content">
 *
 *
 * @package WordPress
 * @subpackage Thomas
 * @since 1.0.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!-- header starts here -->
		<header>
			<div class="wrapper">
				<div class="header-content">
					<?php 
						get_template_part('template-parts/content', 'header-logo'); 
						get_template_part('template-parts/content', 'social-links');
						get_template_part('template-parts/content', 'navigations');
						get_template_part('template-parts/content', 'languages');
						get_template_part('template-parts/content', 'search');
					?>
				</div>
			</div>
		</header>
		<!-- header ends here -->	
		