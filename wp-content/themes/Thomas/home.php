<?php
/**
 * Template Name: Home Template
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @since 1.0.0
 */

get_header();
?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post(); ?>

			<div class="section-wrapper">
				<?php get_template_part('template-parts/pages/home/content', 'hero'); 
				if( have_rows('home_content') ):
		     // loop through the rows of data
		    	while ( have_rows('home_content') ) : the_row();

		        switch (get_row_layout()) :
		        	case 'featured_section':
		        		get_template_part('template-parts/pages/home/content', 'featured'); 
		        		break;
		        	case 'row_with_video':
		        		get_template_part('template-parts/pages/home/content', 'row-with-video'); 
		        		break;
		        	case 'special_section':
		        		get_template_part('template-parts/pages/home/content', 'special'); 
		        		break;	
		        	case 'logo_slider':
		        		get_template_part('template-parts/pages/home/content', 'logo-list'); 
		        		break;
		        	case 'row_with_background':
		        		get_template_part('template-parts/pages/home/content', 'row-with-background'); 
		        		break;
		        	case 'options_list':
		        		get_template_part('template-parts/pages/home/content', 'options-repeater'); 
		        		break;	
		        	case 'search_section':
		        		get_template_part('template-parts/pages/home/content', 'search-section'); 
		        		break;			
		        endswitch;
		     	endwhile;
				 endif;?>
				</div>
			<?php 
			endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();

